import os
import json
   
with open("./devices/devices.json") as jsonFile:
    jsonObject = json.load(jsonFile)
    jsonFile.close()

device_ids = jsonObject["device-ids"]

# Create certificates
for device_id in device_ids:
    os.system('openssl req -outform PEM -x509 -sha256 -nodes -newkey rsa:4096 -keyout ./devices/certificates/' + device_id + '.key.pem -out ./devices/certificates/' + device_id + '.pem -days 365 -extensions usr_cert -subj "/CN=' + device_id + '"')
    os.system("chmod -R 777 ./devices/certificates/" + device_id + ".key.pem")
    os.system("chmod -R 777 ./devices/certificates/" + device_id + ".pem")

