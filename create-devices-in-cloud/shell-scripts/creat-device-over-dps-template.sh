# Create enrollment device 
az iot dps enrollment create -g <ressource group> --dps-name <dps name> --enrollment-id <device id> --attestation-type x509 --certificate-path <path to .pem certificate file>

# Delete enrollment device
az iot dps enrollment delete -g <ressource group> --dps-name <dps name> --enrollment-id <device id>

# Delete iot-device from edge
az iot hub device-identity delete -d <device id> -n <iot hub name> -g <ressource group>
