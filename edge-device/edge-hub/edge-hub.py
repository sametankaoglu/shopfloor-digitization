# -------------------------------- Todos -------------------------------- #


# -------------------------------- Imports -------------------------------- #

import sys
import os
import json
import threading
import time
import paho.mqtt.client as mqtt

from random import randrange
from typing import Any

from azure.iot.device import ProvisioningDeviceClient, X509
from azure.iot.device import IoTHubDeviceClient, MethodResponse
from azure.iot.device.common.models import x509
from azure.iot.device import IoTHubDeviceClient


# -------------------------------- Classes -------------------------------- #

class CIoTDevice:
    def __init__(self, x509_certificate: x509, device_id: str):
        self.x509_certificate           = x509_certificate
        self.device_id                  = device_id
        self.device_client_to_iot_hub   = IoTHubDeviceClient 


# -------------------------------- Constants -------------------------------- #
 



# -------------------------------- Variables -------------------------------- #

__MQTT_client_id                            = os.getenv("MQTT_client_id")
__MQTT_broker_address                       = os.getenv("MQTT_broker_address")
__MQTT_broker_port                          = int(os.getenv("MQTT_broker_port"))
__MQTT_subscribed_topics                    = list((os.getenv("MQTT_subscribed_topics").split(","))) # Add to the array the topics u want to subscribe
__MQTT_own_topic                            = os.getenv("MQTT_own_topic")

__data_to_cloud                             = {} # Json that will be sent to cloud.
__intervall_send_data_to_cloud_in_seconds   = int(os.getenv("intervall_send_data_to_cloud_in_seconds"))
            
__iot_devices                               = list() # Constructor needs to be called
__iot_device_ids                            : list
__iot_device_twins                          : dict
__iot_hub_hostname                          : str
__provisioning_host                         : str
__dps_id_scope                              : str   

# -------------------------------- Functions -------------------------------- #

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

def on_message(client, userdata, msg):
    
    # Prints the topic and message.
    print(msg.topic + " " + str(msg.payload))

    try:
        # It will fail at this point when a number begins with an "+" 
        data = json.loads(msg.payload)
    except:
        # Replace all occupied "+" in the byte array. 
        data = msg.payload.replace(b'+',b'')
        data = json.loads(data)
        
    # Catch messages from topic fischertechnik and send it to iot-hub.
    # When an error occurs get data from iot-device again 
    if msg.topic == "fischertechnik24v" or msg.topic == "plc4x":
        __data_to_cloud.update(data)
        client.publish(__MQTT_own_topic, "message_recieved")

def read_devices_json(path_to_devices_json : str):
    global __iot_device_ids
    global __iot_device_twins
    global __iot_hub_hostname
    global __provisioning_host
    global __dps_id_scope

    with open(path_to_devices_json) as jsonFile:
        device_json = json.load(jsonFile)
        jsonFile.close()

    __iot_device_ids        = device_json["device-ids"]
    __iot_device_twins      = device_json["devices"]
    __iot_hub_hostname      = device_json["iot-hub-hostname"]
    __provisioning_host     = device_json["provisioning_host"]
    __dps_id_scope          = device_json["dps-id-scope"]

def create_iot_devices_with_x509_certificate(path_to_certificates_root_directory : str):
    global __iot_devices
    
    for iot_device_id in __iot_device_ids:
        # Generate x509
        x509 = X509(
            cert_file = path_to_certificates_root_directory + iot_device_id + ".pem",
            key_file = path_to_certificates_root_directory + iot_device_id + ".key.pem"
            )

        # Add new iot-device to array
        __iot_devices.append(CIoTDevice(device_id = iot_device_id, x509_certificate = x509))

def registrate_iot_devices_on_edge_over_dps():
    for iot_device in __iot_devices:
        print("Client init for: " + str(iot_device))

        provisioning_device_client = ProvisioningDeviceClient.create_from_x509_certificate(
            provisioning_host=__provisioning_host,
            id_scope=__dps_id_scope,
            registration_id=iot_device.device_id,
            x509=iot_device.x509_certificate,
        )

        print("Register for: " + str(provisioning_device_client))
        registration_result = provisioning_device_client.register()
        print(registration_result)

def initialize_iot_hub_device_clients():
    for iot_device in __iot_devices:
        iot_device.device_client_to_iot_hub = iothub_client_init(iot_device)

def iothub_client_init(iot_device: CIoTDevice):
    # Create an IoT Hub client
    print("Try to connect to iot-hub")
    
    try:
        device_client = IoTHubDeviceClient.create_from_x509_certificate(
            x509 = iot_device.x509_certificate,
            device_id = iot_device.device_id,
            hostname = __iot_hub_hostname
            )
    except:
        print("Cant connect to iot-hub. iot-device: " + str(iot_device))
        return 

    print("Connected " + str(iot_device) + " to iot-hub")

    return device_client

def send_data_to_iothub_from_iot_device(data: dict, iot_device: CIoTDevice):
    try:
        # Convert json to string
        message = json.dumps(data)

    except:
        print("Cant dump json to string. Data cannot be sent to the iot hub")
        return

    try:
        print("Try to send message to iot-hub")
        iot_device.device_client_to_iot_hub.send_message(message)
        print(str(message))
        print("Message successfully sent")

    except:
        print("Data cannot be sent to the iot hub. Exception raises at iot_hub_client_iot_device.send_message()")

def send_all_data_from_all_iot_devices_to_cloud():
    
    for iot_device in __iot_devices:
        # If device_id exist in data dictionary
        if iot_device.device_id in __data_to_cloud:
            send_data_to_iothub_from_iot_device(__data_to_cloud[iot_device.device_id], iot_device)

def start_twin_and_method_thread(iot_device_twin : dict, iot_device: CIoTDevice):
    try:
        # Start a thread to listen for updates from twin
        twin_update_listener_thread = threading.Thread(target=twin_update_listener, args=(iot_device.device_client_to_iot_hub,))
        twin_update_listener_thread.daemon = True
        twin_update_listener_thread.start()

        print("Sending data as reported property...")

        # Report properties to twin
        print(iot_device_twin)
        iot_device.device_client_to_iot_hub.patch_twin_reported_properties(iot_device_twin)
        print("Reported properties updated")

        # Start a thread to listen for method calls
        device_method_thread = threading.Thread(target=device_method_listener, args=(iot_device.device_client_to_iot_hub,))
        device_method_thread.daemon = True
        device_method_thread.start()
        
    except:
        print("Cant create thread")

def start_twin_updater_thread():
    for iot_device in __iot_devices:
        try:
            # Start a thread to listen for updates from twin
            twin_update_listener_thread = threading.Thread(target=twin_update_listener, args=(iot_device,))
            twin_update_listener_thread.daemon = True
            twin_update_listener_thread.start()

        except:
            print("Cant create twin_updater_thread")

def start_device_method_thread():
    for iot_device in __iot_devices:
        try:      
            # Start a thread to listen for method calls
            device_method_thread = threading.Thread(target=device_method_listener, args=(iot_device,))
            device_method_thread.daemon = True
            device_method_thread.start()
        
        except:
            print("Cant create device_method_thread")

def upload_device_twins_to_cloud():
    # Loop over all iot_devices
    for iot_device in __iot_devices:
        
        # If device_id exist in data dictionary
        if iot_device.device_id in __iot_device_twins:
            
            # Upload device twin to the iot_hub for the device
            print(__iot_device_twins[iot_device.device_id])

            try: 
                iot_device.device_client_to_iot_hub.patch_twin_reported_properties(__iot_device_twins[iot_device.device_id])
            except:
                print("Cant upload device twin to the iot_hub")
                return
            
            print("Upload device twin successfully!")

# Testing
def simulate():
    global __data_to_cloud

    __data_to_cloud["oven"]["wear_saw"] += 1
    __data_to_cloud["oven"]["lamp_wear"] += 1
    __data_to_cloud["vacuumgripper"]["encoder_accumulation_of_rotation"] += 500
    #__data_to_cloud["factory"]["minutes"] += 1
    #__data_to_cloud["sortingline"]["export_white"] = randrange(3)
    #__data_to_cloud["sortingline"]["export_blue"] = randrange(3)
    #__data_to_cloud["sortingline"]["export_red"] = randrange(3)
    #__data_to_cloud["warehouse"]["import_white"] = randrange(3)
    #__data_to_cloud["warehouse"]["import_blue"] = randrange(3)
    #__data_to_cloud["warehouse"]["import_red"] = randrange(3)


# ------------------------------- Thread-Methods -------------------------------- #

def device_method_listener(iot_device):
    while True:
        method_request = iot_device.device_client_to_iot_hub.receive_method_request()
        print (
            "\nMethod callback called with:\nmethodName = {method_name}\npayload = {payload}".format(
                method_name=method_request.name,
                payload=method_request.payload
            )
        )
        if method_request.name == "Maintain-Gripper":
            try:
                # Instert code here to process method
                print("Gripper maintenance method executed")
            except ValueError:
                response_payload = {"Response": "Invalid parameter"}
                response_status = 400
            else:
                response_payload = {"Response": "Executed direct method {}".format(method_request.name)}
                response_status = 200

        elif method_request.name == "Maintain-Saw":
            try:
                # Instert code here to process method
                print("Saw maintenance method executed")
            except ValueError:
                response_payload = {"Response": "Invalid parameter"}
                response_status = 400
            else:
                response_payload = {"Response": "Executed direct method {}".format(method_request.name)}
                response_status = 200

        elif method_request.name == "Maintain-Oven":
            try:
                # Instert code here to process method
                print("Oven maintenance method executed")
            except ValueError:
                response_payload = {"Response": "Invalid parameter"}
                response_status = 400
            else:
                response_payload = {"Response": "Executed direct method {}".format(method_request.name)}
                response_status = 200

        elif method_request.name == "Start-Factory":
            try:
                # Instert code here to process method
                print("Start factory method executed")
            except ValueError:
                response_payload = {"Response": "Invalid parameter"}
                response_status = 400
            else:
                response_payload = {"Response": "Executed direct method {}".format(method_request.name)}
                response_status = 200

        elif method_request.name == "Stop-Factory":
            try:
                # Instert code here to process method
                print("Stop factory method executed")
            except ValueError:
                response_payload = {"Response": "Invalid parameter"}
                response_status = 400
            else:
                response_payload = {"Response": "Executed direct method {}".format(method_request.name)}
                response_status = 200

        else:
            response_payload = {"Response": "Direct method {} not defined".format(method_request.name)}
            response_status = 404

        method_response = MethodResponse(method_request.request_id, response_status, payload=response_payload)
        iot_device.device_client_to_iot_hub.send_method_response(method_response)

def twin_update_listener(iot_device):
    while True:
        patch = iot_device.device_client_to_iot_hub.receive_twin_desired_properties_patch()  # blocking call
        print("Twin patch received:")
        print(patch)


# ------------------------------- Startup -------------------------------- #

# Etablish a connection to the mqtt broker
client              = mqtt.Client(client_id = __MQTT_client_id)
client.on_connect   = on_connect
client.on_message   = on_message
client.connect(__MQTT_broker_address, port = __MQTT_broker_port)

# Subscribe to all topics above given
for topic in __MQTT_subscribed_topics:
    client.subscribe(topic + "/#")

client.loop_start()

read_devices_json(sys.path[0] + "/devices/devices.json")
#read_devices_json(sys.path[0] + "\\..\\devices\\devices.json")
create_iot_devices_with_x509_certificate(sys.path[0] + "/devices/certificates/")
#create_iot_devices_with_x509_certificate(sys.path[0] + "\\..\\devices\\certificates\\")
registrate_iot_devices_on_edge_over_dps()
initialize_iot_hub_device_clients()
upload_device_twins_to_cloud()

# Start threads for the device
start_twin_updater_thread()
start_device_method_thread()



# -------------------------------- Main Loop -------------------------------- #

while True:
    print("Edge-device is running")
    time.sleep(__intervall_send_data_to_cloud_in_seconds)
    print(__data_to_cloud)
    send_all_data_from_all_iot_devices_to_cloud()


