package com.systems;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.entities.PLCVariable;

import org.apache.plc4x.java.PlcDriverManager;
import org.apache.plc4x.java.api.PlcConnection;
import org.apache.plc4x.java.api.exceptions.PlcConnectionException;
import org.apache.plc4x.java.api.messages.PlcReadRequest;
import org.apache.plc4x.java.api.messages.PlcReadResponse;

/**
 * Represents the PLC to that a connection gets etablished.
 */
public class PLC {
    private String          ipAddressOfPlc  = null;
    private PlcConnection   plcConnection   = null;
    
    /**
     * Constructor with arguments. Tries to connect to a plc.
     * @param ipAddressOfPlc
     */
    public PLC(String ipAddressOfPlc) {

        this.ipAddressOfPlc = ipAddressOfPlc;

        // Etablish connection to the plc
        try {
            this.plcConnection = new PlcDriverManager().getConnection(ipAddressOfPlc);
        } catch (PlcConnectionException e) {
            e.printStackTrace();
        }

        // Check if this connection support reading of data.
        if (!plcConnection.getMetadata().canRead()) {
            System.out.println("This connection doesn't support reading.");
        }

    }

    /**
     * Gets values from plc.  
     * @param PLCVariables
     * @return Returns PLCVariables list with filled data attribute for each.
     */
    public List<PLCVariable> getAllData(List<PLCVariable> PLCVariables) {
        
        // Create a new read request
        PlcReadRequest.Builder builder = plcConnection.readRequestBuilder();
        
        // Add those variables to the builder which needs to be readed
        for (PLCVariable plcVariable : PLCVariables) {
            String adress = plcVariable.getLogicalAddress()+":"+plcVariable.getDataType().toUpperCase();
            System.out.println(adress);
            builder.addItem(plcVariable.getName(), adress);    
        }

        // Build the request
        PlcReadRequest readRequest = builder.build();
        
        // Execute request
        PlcReadResponse response = null;
        try {
            response = readRequest.execute().get(10000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        // Set plcVariables data 
        for (PLCVariable plcVariable : PLCVariables) {
            System.out.println(response.getObject(plcVariable.getName()).toString());
            plcVariable.setData(response.getObject(plcVariable.getName()).toString());
        }

        return PLCVariables;
    }

    /**
     * Gets a value from plc.  
     * @param plcVariable
     * @return Returns a PLCVariable with filled data attribute.
     */
    public PLCVariable getSinglePLCVariable(PLCVariable plcVariable) {
        
        // Create a new read request
        PlcReadRequest.Builder builder = plcConnection.readRequestBuilder();
    
        // Add those variables to the builder which needs to be readed
        builder.addItem(plcVariable.getName(), plcVariable.getLogicalAddress());    
    
        // Build the request
        PlcReadRequest readRequest = builder.build();
        
        // Execute request
        PlcReadResponse response = null;
        try {
            response = readRequest.execute().get(10000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        // Set plcVariables data 
        System.out.println(response.getObject(plcVariable.getName()).toString());
        plcVariable.setData(response.getObject(plcVariable.getName()).toString());

        return plcVariable;
    }

    // Getter and Setter

    public String getIpAddressOfPlc() {
        return ipAddressOfPlc;
    }

    public void setIpAddressOfPlc(String ipAddressOfPlc) {
        this.ipAddressOfPlc = ipAddressOfPlc;
    }

    public PlcConnection getPlcConnection() {
        return plcConnection;
    }

    public void setPlcConnection(PlcConnection plcConnection) {
        this.plcConnection = plcConnection;
    }

}
