package com.systems;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.entities.PLCVariable;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Represents a class wich reads an excel file and generates plcVariables from it
 */
public class ExcelReader {
    private String pathToPLCVariables = null;

    /**
     * Constructor
     * @param pathToPLCVariables
     */
    public ExcelReader(String pathToPLCVariables) {
        this.pathToPLCVariables = pathToPLCVariables;
    }

    /**
     * Goes over an excel file which is filled with plcvariables from tiaportal. 
     * Then it generates for each row a plcVariable and push it into the plcVariables list.
     * @return Filled plcVariables list when it got an readable file.
     * @throws IOException If file is not readable etc.
     */
    public List<PLCVariable> readPLCVariables() throws IOException {
        List<PLCVariable> plcVariables  = new ArrayList<>();
        File file                       = new File(this.pathToPLCVariables);
        FileInputStream inputStream     = new FileInputStream(file);
        XSSFWorkbook workbook           = new XSSFWorkbook(inputStream);
        XSSFSheet firstSheet            = workbook.getSheetAt(0);
        Iterator<Row> iterator          = firstSheet.iterator();
        int nameIndex                   = 0;
        int dataTypeIndex               = 0;
        int logicalAddressIndex         = 0;
        int cellIndex                   = 0;
        int rowIndex                    = 0;

        // Go over all rows in a e
        while (iterator.hasNext()) {
            Row             nextRow      = iterator.next();
            Iterator<Cell>  cellIterator = nextRow.cellIterator();

            // Create a plcVariable to set it when it goes over the excel main part
            PLCVariable plcVariable = null;
            if(rowIndex > 0){ 
                plcVariable = new PLCVariable(null, null, null, null, null);
            }
            
            cellIndex = 0;
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                // In Row 0 save the index for the relevant rows
                // Else save relevant cell values in the plcvariable object
                if (rowIndex == 0){
                    
                    if(cell.getStringCellValue().toString().toLowerCase().equals("name")){
                        nameIndex = cellIndex;
                    } 
                    else if(cell.getStringCellValue().toString().toLowerCase().equals("data type")){
                        dataTypeIndex = cellIndex;
                    } 
                    else if(cell.getStringCellValue().toString().toLowerCase().equals("logical address")){
                        logicalAddressIndex = cellIndex;
                    }

                } else {    
                    if(nameIndex == cellIndex){
                        plcVariable.setName(cell.getStringCellValue().replace("analysis_", ""));
                        plcVariable.setStation(plcVariable.getName().split("_", 2)[0]);
                    } else if (dataTypeIndex == cellIndex){
                        plcVariable.setDataType(cell.getStringCellValue());
                    } else if (logicalAddressIndex == cellIndex){
                        plcVariable.setLogicalAddress(cell.getStringCellValue());
                    }
                }
                
                cellIndex++;
            }

            // Add plcVariable to the list
            if(rowIndex > 0){
                plcVariables.add(plcVariable);
            }

            rowIndex++;
        }
         
        workbook.close();
        inputStream.close();

        return plcVariables;
    }

    // Getter and Setter

    public String getPathToPLCVariables() {
        return pathToPLCVariables;
    }

    public void setPathToPLCVariables(String pathToPLCVariables) {
        this.pathToPLCVariables = pathToPLCVariables;
    }
}
