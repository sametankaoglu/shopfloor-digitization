package com.entities;

/**
 * Represents a variable in a plc
 */
public class PLCVariable {
    private String name             = null;
    private String station          = null;
    private String logicalAddress   = null;
    private String dataType         = null;
    private String data             = null;

    /**
     * Constructor of a plcvariable in an excel-table
     * @param name
     * @param station
     * @param logicalAddress
     * @param dataType
     * @param data
     */
    public PLCVariable(String name, String station, String logicalAddress, String dataType, String data) {
        this.name = name;
        this.station = station;
        this.logicalAddress = logicalAddress;
        this.dataType = dataType;
        this.data = data;
    }

    // Getter and Setter
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
  
    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public String getLogicalAddress() {
        return logicalAddress;
    }

    public void setLogicalAddress(String logicalAddress) {
        this.logicalAddress = logicalAddress;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
