package com.service.plc4xapp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import com.entities.PLCVariable;
import com.systems.ExcelReader;
import com.systems.PLC;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONObject;

/**
 * This class represents the main functionality of the plc4x service wich catches the data
 * from the plc in the factory and send it to the edge-device over mqtt.
 * 
 */
public class App {

    public static void main(String[] args) throws MqttException, IOException, InvalidFormatException {
        
        //----------------------------- Variables -----------------------------//
        
        String mqttSubscribedTopic  = System.getenv("mqttSubscribedTopic");
        String mqttClientID         = System.getenv("mqttClientID");
        String mqttOwnTopic         = System.getenv("mqttOwnTopic");
        String ipAddressOfThePlc    = System.getenv("ipAddressOfThePlc");
        String brokerServerID       = System.getenv("brokerServerID");

        //----------------------------- Startup -----------------------------//
        
        //Connect to PLC
        System.out.println("Try to connect to the plc");
        PLC plc = new PLC(ipAddressOfThePlc);
        System.out.println("Successful connected");

        // Read plc-variables.excel file and convert the excel rows to PLCVariable objects
        System.out.println("Try to convert plc-variables from excel to PLCVariable objects");
        //ExcelReader excelReader = new ExcelReader(System.getProperty("user.dir") + "/edge-device/plc4x-service/factory/plc-variables.xlsx"); // Local debugging
        ExcelReader excelReader         = new ExcelReader(System.getProperty("user.dir") + "/factory/plc-variables.xlsx");
        List<PLCVariable> plcVariables  = excelReader.readPLCVariables();
        System.out.println("Successful converted");

        // Connect to Mqtt-Broker
        System.out.println("Try to connect to mqtt broker");
        MqttClient client = new MqttClient(brokerServerID, mqttClientID);
        client.connect();
        client.subscribe(mqttSubscribedTopic + "/#");
        client.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable throwable) { }
      
            @Override
            public void messageArrived(String t, MqttMessage m) throws Exception {
              System.out.println(new String(m.getPayload()));
              // Implement logic to handle messages
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {}
            
        });
        System.out.println("Successful connected");
        
        // Organize the stations
        List<String> stations  = new ArrayList<>();
        for (int i = 0; i < plcVariables.size(); i++) {
            String station = plcVariables.get(i).getStation();
            if (!stations.contains(station)){
                stations.add(station);
            }
        }

        // Generate json for stations 
        JSONObject jsonStations = new JSONObject();
        for (String station : stations) {
            jsonStations.put(station, new JSONObject());   
        }

        //----------------------------- Cleanup -----------------------------//
        // e.g. after ctrl-c     
        
        // Put everthing that needs to be done before shuting the container down in the loop below. 
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable(){
            public void run(){
                try {
                    client.disconnect();
                } catch (MqttException e) {
                    e.printStackTrace();
                }
            }
        }));



        //----------------------------- Main Loop -----------------------------//
        
        while (true) {

            // Get all data from the plc
            plcVariables = plc.getAllData(plcVariables);

            // Fill station json with data
            for (PLCVariable plcVariable : plcVariables) {
                String name             = plcVariable.getName().split("_", 2)[1];
                String data             = plcVariable.getData();
                String station          = plcVariable.getStation();
                JSONObject jsonObject   = jsonStations.getJSONObject(station);
                
                //Delete this. Just for Showcase
                switch (name) {
                    case "runtime_in_seconds":
                        name = "seconds";     
                        break;
                    case "runtime_in_minutes":
                        name = "minutes";
                        break;
                    case "runtime_in_days":
                        name = "days"; 
                        break;
                    case "runtime_in_hours":
                        name = "hours"; 
                        break;
                    case "accumulation_of_gripper_rotation":
                        name = "encoder_accumulation_of_rotation";
                        break;
                    default:
                        break;
                }

                jsonObject.put(name, data); 
            }            
            
            // Send json to the edge-device
            String jsonStringStations   = jsonStations.toString();
            MqttMessage message         = new MqttMessage(jsonStringStations.getBytes());
            System.out.println("Try to send data to the edge-device");
            client.publish(mqttOwnTopic, message);
            System.out.println(jsonStringStations);
            System.out.println("Successful sent");
            System.out.println();

            sleep(4000);
        }
    }



    //----------------------------- Helper functions -----------------------------//

    public static void sleep(int nanos) {
        try {
            Thread.sleep(nanos);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
